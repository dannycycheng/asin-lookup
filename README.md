# ASIN Lookup

A simple REST API built using Python that retrieves product information from Amazon.

## Dependencies
* requests
* lxml
* beautifulsoup4
* Flask

## REST API

```
GET /asin/<asin_id>
```

**Response Body**

```
{  
    "productDescription": "",
    "price": "",
    "asin": ""
}
```