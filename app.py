import html
import json
import re

import requests
from bs4 import BeautifulSoup
from flask import Flask, jsonify

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

@app.route("/asin/<asin_id>")
def main(asin_id):
    product_title, product_price, original_price = None, None, None
    result = {}

    # setup headers to simulate a human
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36",
        "Accept-Encoding": "gzip, deflate",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "DNT": "1",
        "Connection": "close",
        "Upgrade-Insecure-Requests": "1"
    }
    url = "https://www.amazon.ca/dp/{}".format(asin_id)

    try:
        session = requests.Session()
        response = session.get(url, headers=headers)

        if response.status_code != 200:
            return "Unabled to find ASIN: {} from Amazon, please try a different ASIN".format(asin_id), 404

        # Amazon scrambled their html in a way that's difficult
        # for html.parser to parse, so use lxml instead
        bs = BeautifulSoup(response.content, "lxml")
        product_title = bs.select("#productTitle")[0].get_text().strip()
        price_nodes = bs.find_all(
            "span", {"class": re.compile(".*priceBlock.*")})

        for price_node in price_nodes:
            if "priceBlockDealPriceString" in price_node.attrs["class"]:
                product_price = html.unescape(
                    price_node.get_text()).strip().split("\xa0")[1]
            elif "priceBlockBuyingPriceString" in price_node.attrs["class"]:
                product_price = html.unescape(
                    price_node.get_text()).strip().split("\xa0")[1]
            elif "priceBlockSalePriceString" in price_node.attrs["class"]:
                product_price = html.unescape(
                    price_node.get_text()).strip().split("\xa0")[1]
            elif "priceBlockStrikePriceString" in price_node.attrs["class"]:
                original_price = price_node.get_text().split("\xa0")[1]

        result["productDescription"] = product_title
        result["asin"] = asin_id
        result["price"] = product_price

    except requests.RequestException:
        return "Failed to get results from Amazon, try again later...", 503
    except Exception as e:
        return "Something terribly happened, whoever wrote this code is an idiot!\n\n{}".format(e), 500

    return jsonify(result), 200